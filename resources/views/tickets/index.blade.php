@extends('layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
<div class="container-fluid">
        <!-- Succes message -->
        @if(session('message'))
            <div class="alert alert-danger">
                {{session('message')}}
            </div>
        @endif
    <!-- Error message -->
        @if(count($errors)>0)
            @foreach($errors->all() as $error)
                <div class="alert alert-danger">
                    {{$error}}
                </div>
            @endforeach
        @endif
        <div class="card">
            <div class="card-header">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h4>Recent Tickets</h4>
                            @if($tickets->count()>0)
                                @foreach($tickets as $ticket)

                                <div class="row">
                                  <div class="col-sm-8">
                                    <div class="post">
                                        <div class="user-block">
                                            <img class="img-circle img-bordered-sm" src="{{asset('assets/img/avatar/avatar2.png')}}" alt="user image">
                                            <span class="username"><a href="#">{{$ticket->user->name}}</a></span>
                                            <span class="description">Shared publicly - {{ $ticket->created_at }}</span>
                                        </div>
                                        <!-- /.user-block -->
                                        <p>{{$ticket->desc}}</p>

                                        <p>
                                            <a href="{{route('tickets.show', $ticket)}}" class="link-black text-sm"><i class="fas fa-link mr-1"></i> {{ $ticket->number }} </a>
                                        </p>
                                        <p>
                                            <a class="btn btn-info btn-sm" href="{{route('tickets.show', $ticket)}}">
                                                <i class="fas fa-pencil-alt">
                                                </i>
                                                OPEN
                                            </a>
                                        </p>
                                    </div>
                                  </div>

                                  <div class="col-sm-4">
                                    <table class="table">

                                        <tbody>
                                            <tr>
                                                <td class="project-state">
                                                    <span class="badge badge-success">
                                                        {{ $ticket->status }}
                                                    </span>
                                                </td>

                                                <td class="project-actions text-right">
                                                    @if($ticket->status == 'Pending')
                                                    <form action="{{ route('tickets.accepted', $ticket )}}" method="post">
                                                        @method('PATCH')
                                                        @csrf
                                                        <button type="submit" class="btn btn-info btn-sm"> {{ __('Accept') }}</button>
                                                    </form>
                                                    @endif
                                                    @if($ticket->status == 'In Progress')
                                                    <form action="{{ route('tickets.completed', $ticket )}}" method="post">
                                                        @method('PATCH')
                                                        @csrf
                                                        <button type="submit" class="btn btn-default btn-sm"> {{ __('Complited') }}</button>
                                                    </form>
                                                    @endif
                                                    @if($ticket->status == 'In Progress' || $ticket->status == 'Pending')
                                                    <form action="{{ route('tickets.rejected', $ticket )}}" method="post">
                                                        @method('PATCH')
                                                        @csrf
                                                        <button type="submit" class="btn btn-danger btn-sm"> {{ __('Reject') }}</button>
                                                    </form>
                                                    @endif
                                                </td>

                                                <td class="project-actions text-right">
                                                    <a class="btn btn-danger btn-sm" href="{{route('tickets.destroy', $ticket)}}">
                                                        <i class="fas fa-trash">
                                                        </i>
                                                        DELETE
                                                    </a>
                                                </td>

                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                            <hr/>

                            @endforeach
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</section>

@endsection
