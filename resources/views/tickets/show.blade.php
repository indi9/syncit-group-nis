@extends('layouts.app')

@section('content')

<section class="content">

    <!-- Default box -->
    <div class="card">
        <div class="col-sm-8">
            <div class="card-header">
                <div><h3 class="card-title">{{$ticket->number}}</h3></div>
                <div class="post">
                    <div class="user-block">
                        <img class="img-circle img-bordered-sm" src="{{asset('assets/img/avatar/avatar2.png')}}" alt="user image">
                        <span class="username"><a href="#">{{$ticket->user->name}}</a></span>
                        <span class="description">Ticket created at::  {{ $ticket->created_at }}</span>
                    </div>
                </div>
            </div>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fas fa-times"></i></button>
                    </div>
                </div>

                <div class="card-body">
                    {{$ticket->desc}}
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    @if($ticket->supportID != null)
                    IT Support: <span class="">{{$ticket->supportName}}</span>
                    <div class="post">
                        <div class="user-block">
                            <img class="img-circle img-bordered-sm" src="{{asset('assets/img/avatar/avatar.png')}}" alt="user image">
                            <span class="username"><a href="#">{{$ticket->supportEmail}}</a></span>
                            <span class="description">Ticket created at::  {{ $ticket->updated_at }}</span>

                            <table class="table">

                                <tbody>
                                <tr>
                                    <td class="project-state">
                                        <span class="badge badge-success">
                                            {{ $ticket->status }}
                                        </span>
                                    </td>
                                    <td>
                                        <p>
                                            <a class="btn btn-info btn-sm" href="{{route('tickets.show', $ticket)}}">
                                                <i class="fas fa-pencil-alt">
                                                </i>
                                                WRITE Comment
                                            </a>
                                        </p>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <!-- /.card-footer-->
    </div>
    <!-- /.card -->

</section>

@endsection