@extends('layouts.app')

@section('content')
    <div class="card-body">
        <form action="{{ route('tickets.update', $ticket )}}" method="post">
            @method('PATCH')
            @csrf

            <div class="row">

                <div class="col-sm-6">
                    <!-- text input -->
                    <div class="form-group">
                        <label>TICKET NUMBER</label>
                        <input type="text" name="number" value="{{$ticket->number}}" class="form-control" placeholder="Enter ..." disabled>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Select Status</label>
                        <select class="form-control" name="status">
                            <option value="{{$ticket->status}}" selected>{{$ticket->status}}</option>
                            <option value="Pending">Pending</option>
                            <option value="In Progress">In Progress</option>
                            <option value="Rejected">Rejected</option>
                            <option value="Completed">Completed</option>
                        </select>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Descriprion of Ticket</label>
                        <textarea class="form-control" name="desc" rows="3" placeholder="Enter ..." disabled="">{{$ticket->desc}}"</textarea>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>TICKET NUMBER</label>
                        <input type="text" name="number" value="{{$user->name}}" class="form-control" placeholder="Enter ..." disabled>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-info">Accepted Ticket</button>

        </form>

        <hr/>
        <br/>
            <td class="project-actions text-right">

                <form action="{{ route('tickets.accepted', $ticket )}}" method="post">
                    @method('PATCH')
                    @csrf
                    <button type="submit" class="btn btn-info btn-sm"> {{ __('Accept Ticket') }}</button>
                </form>
                <form action="{{ route('tickets.completed', $ticket )}}" method="post">
                    @method('PATCH')
                    @csrf
                    <button type="submit" class="btn btn-default btn-sm"> {{ __('Complited Ticket') }}</button>
                </form>
                <form action="{{ route('tickets.rejected', $ticket )}}" method="post">
                    @method('PATCH')
                    @csrf
                    <button type="submit" class="btn btn-danger btn-sm"> {{ __('Reject Ticket') }}</button>
                </form>
            </td>



    </div>

@endsection
