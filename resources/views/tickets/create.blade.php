@extends('layouts.app')

@section('content')

<div class="card-body">
    <form role="form" method="post" action="{{ route('tickets.store') }}" enctype="multipart/form-data" >
        @csrf

        <div class="row">
            <div class="col-sm-6">
                <!-- textarea -->
                <div class="card-footer">
                    <div class="form-group">

                        <label>New Ticket</label>
                        <textarea name="desc" rows="3" class="form-control {{ $errors->has('desc') ? 'is-danger' : '' }}"  placeholder="Enter Problem ...."></textarea>
                        @error('desc')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $desc }}</strong>
                        </span>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-info">Create</button>

               </div>
            </div>
        </div>

    </form>
</div>

@endsection