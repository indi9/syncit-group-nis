<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="info">
                 <a class="d-block">User : {{ auth()->user()->email }}<a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <li class="nav-item">
                <a href="{{route('tickets.index')}}" class="nav-link">
                    <i class="fas fa-circle nav-icon"></i>
                    <p>All New & Open</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('tickets.myNewAndOpen')}}" class="nav-link">
                    <i class="fas fa-circle nav-icon"></i>
                    <p>My New & Open</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('tickets.allPending')}}" class="nav-link">
                    <i class="fas fa-circle nav-icon"></i>
                    <p>All Pending</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('tickets.allClosed')}}" class="nav-link">
                    <i class="fas fa-circle nav-icon"></i>
                    <p>All Closed</p>
                </a>
            </li>

            <li class="nav-item">
                <a href="{{route('tickets.create')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>New Ticket</p>
                </a>
            </li>

            <hr/>

            <li class="nav-item">
                <a href="{{route('logout')}}" class="nav-link" onclick="event.preventDefault();
                                                                   document.getElementById('logout-form').submit();">
                    <i class="far fa-circle nav-icon"></i>
                    <p> {{ __('Logout') }} </p>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>