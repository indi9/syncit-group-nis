<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\PageController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// User *****************************************************************************************************************************************************

Route::get('/welcome', [PageController::class, 'welcome'])->name('welcome');
Route::get('/contact', [PageController::class, 'contact'])->name('contact');
Route::post('/contact/send', [PageController::class,'contactSendEmail'])->name('contact.send');

Auth::routes();
Auth::routes(['verify' => true]);

//  Must be verified user
Route::get('/', [HomeController::class, 'home'])->name('home');  // all tickets of user

Route::prefix('tickets')->group(function () {
    
    // Route::resource('/tickets', TicketController::class);
    Route::get('/',[TicketController::class, 'index'])->name('tickets.index');
    Route::get('/create',[TicketController::class, 'create'])->name('tickets.create');
    Route::post('/store',[TicketController::class, 'store'])->name('tickets.store');
    Route::get('/{ticket}',[TicketController::class, 'show'])->name('tickets.show');
    Route::delete('/{ticket}',[TicketController::class, 'destroy'])->name('tickets.destroy');

    // Status of Ticket
    Route::patch('/{ticket}/accepted', [TicketController::class, 'acceptTicket'])->name('tickets.accepted');
    Route::patch('/{ticket}/rejected', [TicketController::class, 'rejectedTicket'])->name('tickets.rejected');
    Route::patch('/{ticket}/completed', [TicketController::class, 'completedTicket'])->name('tickets.completed');

    // Ticket selection view
    //Route::get('/tickets/allNewAndOpen', [TicketController::class, 'allNewAndOpen'])->name('tickets.unreads');
    Route::get('/my_new_and_open', [TicketController::class, 'myNewAndOpen'])->name('tickets.myNewAndOpen');
    Route::get('/all_pending', [TicketController::class, 'allPending'])->name('tickets.allPending');
    Route::get('/all_closed', [TicketController::class, 'allClosed'])->name('tickets.allClosed');


});


    //Users
    //Route::resource('/users', 'App\Http\Controllers\UserController');
