<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'number','status', 'desc', 'supportID','supportEmail', 'supportName',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function documents()
    {
        return $this->hasMany(Document::class);
    }

    public function addDocument($atributes){
        return $this->documents()->create($atributes);
    }

}
