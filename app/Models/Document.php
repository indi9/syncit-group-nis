<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    use HasFactory;

    protected $fillable = [
        'ticket_id', 'name', 'dir', 'image',
        ];


    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }
}
