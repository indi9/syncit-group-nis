<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ticket;
use Auth;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function home()
    {
        $user = Auth::user();
        if($user->is_admin)  {
            // dd("YOU ARE ADMIN");
            $tickets = Ticket::where('status','!=', 'Completed')->orderBy('created_at', 'desc')->get();
            return view('home', compact('tickets'));
        }
        //dd("NOT ADMIN!!!!!!!");
        $tickets = Ticket::where('status','!=', 'Completed')->where('user_id', '=', $user->id)->orderBy('created_at', 'desc')->get();
        return view('home', compact('tickets'));

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
    public function dashboard()
    {
        return view('admins.dashboard');
    }

     */
}
