<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTicketRequest;
use App\Http\Requests\UpdateTicketRequest;

use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use File;

use App\Models\Ticket;
use App\Models\User;


class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     *
     *
     */

    public function __construct()
    {
        $this->middleware(['auth'=>'verified']);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tickets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTicketRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTicketRequest $request)
    {
        if(Auth::user()){
            dd($request->all());
            $attributes = request()->validate( [
                'user_id' => Auth::user()->id,
                'number' => "T-".uniqid(),
                'desc' => ['required', 'string', 'max:1000'],
            ]);
            //dd($attributes);
            $task = Ticket::create($attributes);
        }

        if($task){
            return redirect()->route('home')->with('message', 'New ticket is created!!');
        }
        return back()->with('message', 'New ticket is not created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        //dd($ticket);
        if(Auth::user()->id == $ticket->user_id || Auth::user()->is_admin ){
            return view('tickets.show', compact('ticket'));
        }
        return back()->with('message', 'Is not your ticket.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        if(Auth::user()->id == $ticket->user_id || Auth::user()->is_admin){
            $user = User::where('is_admin', true)->where('id', Auth::user()->id)->first();
            //dd($user);
            return view('tickets.edit', compact('ticket','user'));
        }
        return back()->with('message', 'Is not your ticket.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTicketRequest  $request
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTicketRequest $request, Ticket $ticket)
    {
        /**
        if(Auth::user()->id == $ticket->user_id && $ticket->status == 'Pending'){

             $attributes = request()->validate([
                'desc' => ['required', 'string', 'min:1', 'max:100'],
             ]);
        }
        */
        if(Auth::user()->is_admin) {
            $user = Auth::user();
            $attributes = request()->validate([
                'status' => ['required', 'string'],
                'supportID' => $user->id,
                'supportEmail' => $user->email,
                'supportName' => $user->name,
            ]);
            $ticket->update($attributes);  //auth()->user()->
        }
         return view('tickets.index', 'ticket');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        if(Auth::user()->id == $ticket->user_id && $ticket->status == 'Pending' || Auth::user()->is_admin){

            $ticket->delete();
            return view('tickets.index', 'ticket')->with('message', 'Ticket is deleteed.');
        }
        return back()->with('message', 'Not deleted.');
    }

    public function acceptTicket(Ticket $ticket)
    {
        if(Auth::user()->is_admin) {
            $user = Auth::user();
            $ticket->update([
                'status' => 'In Progress',
                'supportID' => $user->id,
                'supportEmail' => $user->email,
                'supportName' => $user->name,
            ]);
        }
        return back()->with('message', 'Ticket accepted.');
    }

    public function rejectedTicket(Ticket $ticket)
    {
        if(Auth::user()->is_admin) {
            $user = Auth::user();
            $ticket->update([
                'status' => 'Rejected',
                'supportID' => $user->id,
                'supportEmail' => $user->email,
                'supportName' => $user->name,
            ]);
        }
        return back()->with('message', 'Ticket rejected.');
    }

    public function completedTicket(Ticket $ticket)
    {
        if(Auth::user()->is_admin) {
            $user = Auth::user();
            $ticket->update([
                'status' => 'Completed',
                'supportID' => $user->id,
                'supportEmail' => $user->email,
                'supportName' => $user->name,
            ]);
        }
        return back()->with('message', 'Ticket accepted.');
    }

    //----------------------------------------------------------------------------------
    public function index() //allNewAndOpen
    {
        if (Auth::user()){
            $tickets = Ticket::where('status','!=', 'Completed')->orderBy('created_at', 'desc')->get();
        }
        return view('tickets.index', compact('tickets'));
    }
/**
    public function allNewAndOpen()
    {
        $user = Auth::user();
        if ($user->is_admin == false){
            $tickets = Ticket::where('status','!=', 'Completed')->where('user_id', '=', $user->id)->orderBy('created_at', 'desc')->get();
        }
        if ($user->is_admin == true){
            $tickets = Ticket::where('status','!=', 'Completed')->orderBy('created_at', 'desc')->get();
        }
        return view('tickets.index', compact('tickets'));
    }
  */
    public function myNewAndOpen()
    {
        $user = Auth::user();
        if ($user->is_admin == false){
            $tickets = Ticket::where('status','!=', 'Completed')->where('user_id', '=', $user->id)->orderBy('created_at', 'desc')->get();
        }
        if ($user->is_admin == true){
            $tickets = Ticket::where('status','!=', 'Completed')->where('supportID', '=', $user->id)->orderBy('created_at', 'desc')->get();
        }
        return view('tickets.index', compact('tickets'));
    }
    public function allPending()
    {
        $user = Auth::user();
        if ($user->is_admin == false){
            $tickets = Ticket::where('status','Pending')->where('user_id', '=', $user->id)->orderBy('created_at', 'desc')->get();
        }
        if ($user->is_admin == true){
            $tickets = Ticket::where('status','Pending')->orderBy('created_at', 'desc')->get();
        }
        return view('tickets.index', compact('tickets'));
    }
    public function allClosed()
    {
        $user = Auth::user();
        if ($user->is_admin == false){
            $tickets = Ticket::where('status','Completed')->where('user_id', '=', $user->id)->orderBy('created_at', 'desc')->get();
        }
        if ($user->is_admin == true){
            $tickets = Ticket::where('status','Completed')->orderBy('created_at', 'desc')->get();
        }
        return view('tickets.index', compact('tickets'));
    }


}
