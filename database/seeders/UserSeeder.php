<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $password = Hash::make('1234567890');

        $user = [
                  [
                    'email'=>'support1@test.com',  // admin1@gmail.com/secret
                    'name'=>'Admin Adminovic',
                    'is_admin'=>true,
                    'password'=> bcrypt('secret'),
                    'email_verified_at' => now(),
                  ],

                  [
                    'email'=>'support2@test.com',  // admin1@gmail.com/secret
                    'name'=>'Suport Suportovic',
                    'is_admin'=>true,
                    'password'=> bcrypt('secret'),
                    'email_verified_at' => now(),
                  ],

                  [
                    'name'=>'User1 Userovic1',
                    'email'=>'user1@test.com',
                    'is_admin'=>false,
                    'password'=> $password,
                    'email_verified_at' => now(),
                  ],

                  [
                       'name'=>'User2 2',
                       'email'=>'user2@test.com',
                       'is_admin'=>false,
                       'password'=> $password,
                       'email_verified_at' => now(),
                   ],

        ];

        foreach ($user as $key => $value) {
            User::create($value);
        }

        for ($i = 0; $i < 100; $i++) {
            $email = $faker->unique()->email;
            User::create([
                'email' => $email,
                'name' => $faker->name,
                'is_admin'=>false,
                'password' => $password,
                'email_verified_at' => now(),
            ]);
        }

    }
}
