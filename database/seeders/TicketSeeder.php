<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Ticket;
use App\Models\User;


class TicketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$user_ids = User::Where('is_admin', false)->get();

        $faker = \Faker\Factory::create();
        for ($i = 1; $i < 300; $i++) {

            Ticket::create([
                'number' => "T-" . uniqid(),
                'user_id' => random_int(3,50), //$user_ids[array_rand($user_ids)],
                'desc' => $faker->text($maxNbChars = 300),
                'created_at' => $faker->datetime,
            ]);
        }
    }

}
